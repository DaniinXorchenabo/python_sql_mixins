from __future__ import annotations

import functools
from collections import namedtuple
from typing import Type, Optional, List, Union, Any, Tuple, Set, Dict, TypeVar, Callable

# noinspection PyPackageRequirements
# from gino.crud import CRUDModel
# noinspection PyPackageRequirements
from pydantic import BaseModel
# noinspection PyProtectedMember,PyPackageRequirements
from pydantic.main import ModelMetaclass
# noinspection PyPackageRequirements
from sqlalchemy import text
# noinspection PyPackageRequirements
from sqlalchemy.dialects.postgresql import insert
# noinspection PyProtectedMember,PyPackageRequirements
from sqlalchemy.engine import Row as RowProxy
# noinspection PyPackageRequirements
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncConnection
from sqlalchemy.orm import query as sqla_query
from sqlalchemy.orm.decl_api import DeclarativeMeta

# from .guid import
from .guid import return_uuid
from .sa_types import select, update, delete

__all__ = [
    "BaseDBModelMixin",
    "PydanticGINOMixin",
    "comparable",
]



PydanticModel = TypeVar("PydanticModel", bound=BaseModel)
B = TypeVar("B")

comparable = namedtuple(
    "comparable",
    (
        "sign",
        "other",
    ),
)


class BaseDBModelMixin:
    """
    This is a template class  for Mixin inheritance.
    Each of its functions must be overloaded;

    The _serialize decorator must be explicitly inherited as attr:
    _serialize = BaseDBModelMixin._serialize or BaseDBModelMixin._serialize_async;

    The _bind attribute must explicitly be assigned a class that can handle data in your Mixin
    AND must be assigned to real database Model or Table or whatever in the derived class.

    BE AWARE! Mixin cannot provide typing information, explicit typing is required in inherited classes,
    or explicit type specification when assigning method result to variables.
    """

    # in case of inheritance must be typed manually like this:
    # _bind: YourModelDB = YourModelDB
    _bind = None

    def _serialize(wrapped_function):
        """This decorator should be overloaded in order to provide auto-[de]serialization between(after) methods"""

        def wrapper(self, *args, **kwargs):
            """This is just an example"""

            result = wrapped_function(self, *args, **kwargs)
            return self._serialize_method(self._deserialize_method(result))

        return wrapper

    def _serialize_method(self, *args, **kwargs) -> Any:
        """This method should be overloaded in order to serialize your Model using needed args or kwargs."""
        pass

    def _deserialize_method(self, *args, **kwargs) -> Any:
        """This method should be overloaded in order to retrieve data from database."""
        pass

    @property
    def _data_source(self): # noqa
        """This property or method must provide object data to perform operations on it."""
        pass

    _serialize = staticmethod(_serialize)


# noinspection SpellCheckingInspection
def pydantic_gino_serializer(wrapped_function=None, *, sync=False, clsmethod=False):
    """
    This decorator is necessary to simplify the serialization of data in each function that works with them.
    If you do not directly inherit it as described in the class docstrings,
    the task of processing the data and returning the desired class remains with you.

    """

    if wrapped_function is None:
        return functools.partial(pydantic_gino_serializer, sync=sync, clsmethod=clsmethod)

    if sync is False:
        # noinspection PyProtectedMember
        @functools.wraps(wrapped_function)
        async def wrapper(self_or_cls, *args, **kwargs):
            result = await wrapped_function(self_or_cls, *args, **kwargs)
            if clsmethod is True:
                return self_or_cls._serialize_method(self_or_cls, self_or_cls._deserialize_method(result))

            return self_or_cls._serialize_method(self_or_cls._deserialize_method(result))

        return wrapper

    else:
        # noinspection PyProtectedMember
        @functools.wraps(wrapped_function)
        def wrapper(self_or_cls, *args, **kwargs):
            result = wrapped_function(self_or_cls, *args, **kwargs)
            if clsmethod is True:
                return self_or_cls._serialize_method(self_or_cls, self_or_cls._deserialize_method(result))

            return self_or_cls._serialize_method(self_or_cls._deserialize_method(result))

        return wrapper


# noinspection PyProtectedMember
class PydanticGINOMixin(BaseDBModelMixin):
    _bind = B
    _serialize = pydantic_gino_serializer

    _default_primary_key_attr: str = "id"  # in order to exclude unexpected exceptions
    _pk_creation_method: Callable = return_uuid  # uuid.uuid4

    @classmethod
    def get_bind(cls) -> B:
        return cls._bind  # noqa

    @classmethod
    def get_engine(cls) -> AsyncEngine:
        return cls._bind.metadata.bind  # noqa

    @classmethod
    def _implicit_pk_creation(cls, **fields):

        if cls._default_primary_key_attr is not None and cls._pk_creation_method is not None:

            fields[cls._default_primary_key_attr] = (
                fields.get(cls._default_primary_key_attr) or cls._pk_creation_method()
            )

            return fields

    @classmethod
    def _massive_implicit_pk_creation(cls, values: List[dict]):

        # if one have id - all must have id's
        if len(values) > 0:
            example = values[0]
            if cls._default_primary_key_attr in example.keys():
                if example[cls._default_primary_key_attr] is not None:
                    return values

        # otherwise we should fill all of them with id's
        return [cls._implicit_pk_creation(**v) for v in values]

    def _serialize_method(self, values: Union[Dict, List[Dict]]):

        if values is None:
            return None

        if isinstance(self, ModelMetaclass):  # if we got no instance, but class type
            cls = self
            if isinstance(values, list):
                return list(map(lambda x: cls(**x), values)) # noqa
            return cls(**values) # noqa

        if isinstance(values, list):
            return list(map(lambda x: self.__class__(**x), values))  # noqa
        return self.__class__(**values)  # noqa

    # noinspection PyTypeChecker,PyArgumentList
    @staticmethod
    def _deserialize_method(
        raw: Union[RowProxy, List[RowProxy]]
    ) -> Union[Dict, List[Dict]]:

        if isinstance(raw, list):
            if len(raw) > 0:
                example = raw[0]
            else:
                # raise ValueError("Provided list can't be empty.")
                return []
        else:
            example = raw

        if issubclass(example.__class__, RowProxy) or isinstance(example.__class__, ModelMetaclass):
            if isinstance(raw, list):
                return list(map(dict, raw))
            return dict(example)

        elif issubclass(example.__class__, DeclarativeMeta):
            if isinstance(raw, list):
                return list(map(lambda x: x.to_dict(), raw))
            return example.to_dict()

        else:
            if example is not None:  # some funcs have Optional return
                raise TypeError(
                    f"Unable deserialize object {example} - "
                    f"type should be RowProxy or subclass of declarative base, not {example.__class__}"
                )

    @property
    def _data_source(self):
        return self.dict(exclude_unset=True) # noqa

    @classmethod
    def _add_where(cls, existing_query, where: Dict[str, Any]) -> sqla_query.Query:
        # TODO Попытаться сделать column_expressions для поддержки знаков кроме ==
        #  dict(column='state', operand='>=', value='0')
        #  можно попытаться делать кучу if и добавлять where по операнду,
        #  либо вручную делать список сравнений, что очень вряд ли будет работать
        #  comparisons=[Model.state >= 0]

        return_query = existing_query

        for column, value in where.items():  # getting columns and adding expressions to query
            if isinstance(value, bool) or value is None:
                return_query = return_query.where(getattr(cls._bind, column).is_(value))

            elif isinstance(value, comparable):
                return_query = return_query.where(text(f"{str(getattr(cls._bind, column))} {value.sign} {value.other}"))

            elif isinstance(value, list) or isinstance(value, tuple) or isinstance(value, set):
                return_query = return_query.where(getattr(cls._bind, column).in_(value))

            else:
                return_query = return_query.where(getattr(cls._bind, column) == value)

        return return_query

    # def dict(self):
    #     """Placeholder for correct inheritance with pydantic.BaseModel"""
    #     pass

    def set_attr(self, attr: str, value: Any) -> PydanticGINOMixin:
        """
        Chain method allows to setup some attr "in place" before adding to db, updating etc

        :param attr: name of the attr
        :param value: value for the attr
        :return: serialized model for next operations
        """
        self.__setattr__(attr, value)

        return self

    def create_id(
        self,
        *,
        attr_name: str = _default_primary_key_attr,
        value: Optional[Any] = None,
        creation_method=_pk_creation_method,
        create_args: Optional[list] = None,
        create_kwargs: Optional[dict] = None,
    ) -> PydanticGINOMixin:
        """
        Chain method which setup id attribute and returns same model with new attr

        :param attr_name: name of the attr which represents "id" primary key
        :param value: value which should be used as "id", otherwise uses "creation_method"
        :param creation_method: method which should provide unique "id"
        :param create_args:
        :param create_kwargs:
        :return: serialized model for next operations
        """
        create_args = create_args or []
        create_kwargs = create_kwargs or {}

        if value is None:  # Checking for method which must create id if value is not provided
            if creation_method is not None:
                return self.set_attr(attr_name, creation_method(*create_args, **create_kwargs))
            else:
                raise AttributeError(f'Unable to create id because "creation_method" or value is not provided')
        else:
            return self.set_attr(attr_name, value)

    @classmethod
    async def create(cls, check_for_pk: bool = True, **values) -> PydanticGINOMixin:
        """
        Create exactly one row using provided values without creating instance

        :param values: named arguments which should be used to create row
        :return: serialized model
        """

        return await cls._create_without_instance(check_for_pk, **values)

    @classmethod
    @_serialize(clsmethod=True)
    async def _create_without_instance(cls, check_for_pk: bool = True, **values) -> PydanticGINOMixin:

        if check_for_pk and cls._default_primary_key_attr is not None and cls._pk_creation_method is not None:
            values = cls._implicit_pk_creation(**values)

        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                result = (await conn.execute(insert(cls._bind).values(**values).returning(cls._bind))).first()
        await conn.close()
        return result

    async def create_one_self(
        self,
        check_for_pk: bool = True,
    ) -> PydanticGINOMixin:
        """Create one row using the values contained in the class instance"""

        return await self._create_without_instance(check_for_pk, **self._data_source)

    @classmethod
    async def insert_many(
        cls,
        *,
        values_list: Union[List["Any"], List[Dict]],
        with_return: bool = False,
        on_conflict_do: str = "nothing",
        check_for_pk: bool = True,
        raise_on_exc: bool = False,
    ) -> List[Any]:  # PydanticGINOMixin
        """
        Insert as many rows as you pass to func

        :param values_list: homogeneous, uniform list of "models" or "dicts", not either.
        :param with_return: if "True", returns updated serialized model
        :param on_conflict_do: allows to choose update strategy:
         "nothing" - just skip values that causes conflicts
         "update" - replace data in rows with new values
         "reject" - allows to raise Exceptions from asyncpg or SQLAlchemy just skipping the "ON CONFLICT" clause
        :param check_for_pk: if True, check models default_primary_key_attr for None and create key using the
        pk_creation_method
        :param raise_on_exc:
        :return: list of serialized models
        """

        return await cls._insert_many_dispatcher(
            values_list=values_list,
            with_return=with_return,
            on_conflict_do=on_conflict_do,
            check_for_pk=check_for_pk,
            raise_on_exc=raise_on_exc,
        )

    @classmethod
    async def _insert_many_dispatcher(
        cls,
        values_list: Union[List["Any"], List[Dict]],
        with_return: bool = False,
        on_conflict_do: str = "nothing",
        check_for_pk: bool = True,
        raise_on_exc: bool = False,
    ) -> List[Any]:  # PydanticGINOMixin

        if not values_list:  # means values_list == []
            if raise_on_exc is False:
                return []
            raise ValueError('Unable to insert rows: provided "values_list" can\'t be empty.')

        if not isinstance(values_list, list):
            raise ValueError(f'Provided values variable must be "list", not {type(values_list)}.')

        example = values_list[0]
        if not (isinstance(example, BaseModel) or isinstance(example, dict)):
            raise ValueError(f'Provided values must have types "BaseModel" or "dict", not {type(example)}.')

        if isinstance(example, BaseModel):
            items_length = len(example.dict().items())
        else:
            items_length = len(example.items())

        values_list_length = len(values_list)

        result_list: List[PydanticGINOMixin] = []

        exc = None
        async with cls.get_engine().connect() as connection:
            async with connection.begin() as tx:
                try:

                    if values_list_length * items_length > 32767:

                        # calculating optimal number of inserted models per one insert
                        optimal_chunk_size = values_list_length // (values_list_length * items_length // 32767 + 1)

                        # setting up offsets
                        left_offset = 0
                        right_offset = int(optimal_chunk_size)

                        while left_offset < values_list_length:
                            temp = await cls._insert_many_executor(
                                connection,
                                values_list[left_offset: min(right_offset, values_list_length)],
                                with_return,
                                on_conflict_do,
                                raise_on_exc,
                            )

                            if with_return:
                                result_list.extend(temp)

                            left_offset += optimal_chunk_size
                            right_offset += optimal_chunk_size

                    else:
                        result_list = await cls._insert_many_executor(
                            connection, values_list, with_return, on_conflict_do, check_for_pk, raise_on_exc
                        )

                except Exception as exc_inner:
                    exc = exc_inner
                    await tx.rollback()

        await connection.close()

        if exc is not None:
            raise exc

        return result_list

    @classmethod
    @_serialize(clsmethod=True)
    async def _insert_many_executor(
        cls,
        connection: AsyncConnection,
        values_list: Union[List["Any"], List[Dict]],
        with_return: bool = False,
        on_conflict_do: str = "nothing",
        check_for_pk: bool = True,
        raise_on_exc: bool = False,
    ) -> List[Any]:  # PydanticGINOMixin

        if isinstance(values_list[0], BaseModel):
            values_list = list(map(lambda x: x.dict(exclude_unset=True), values_list))

        if check_for_pk and cls._pk_creation_method is not None and cls._default_primary_key_attr:
            values_list = cls._massive_implicit_pk_creation(values_list)

        query = insert(cls._bind, values=values_list)
        if with_return:
            query = query.returning(cls._bind)

        # resolving on_conflict clause
        if on_conflict_do == "nothing":
            query = query.on_conflict_do_nothing()
        elif on_conflict_do == "update":
            set_statement = {column.name: excluded for column, excluded in zip(cls._bind.__table__.c, query.excluded)}
            query = query.on_conflict_do_update(constraint=f"{cls._bind.__table__}_pkey", set_=set_statement)
        elif on_conflict_do == "reject":
            pass
        else:
            raise ValueError('The only possible values is "nothing", "update" or "reject".')

        # doing an action
        if with_return:
            inserted_rows = (await connection.execute(query)).all()
        else:
            inserted_rows = None
            await connection.execute(query)

        return inserted_rows if inserted_rows else None

    @classmethod
    async def insert_one(
        cls,
        *,
        value: Union[Type[BaseModel], Any, Dict],
        with_return: bool = False,
        on_conflict_do: str = "nothing",
    ) -> Optional[PydanticGINOMixin]:
        """
        Insert exactly one row | value=BaseModel or dict, possible values of "on_conflict_do=": nothing, update, reject

        :param value: Model or dict
        :param with_return: if "True", returns updated serialized model
        :param on_conflict_do: allows to choose update strategy:
         "nothing" - just skip values that causes conflicts
         "update" - replace data in rows with new values
         "reject" - allows to raise Exceptions from asyncpg or SQLAlchemy just skipping the "ON CONFLICT" clause
        :return: serialized model
        """

        result = await cls.insert_many(values_list=[value], with_return=with_return, on_conflict_do=on_conflict_do)

        if with_return is True:
            if len(result) == 1:
                return result[0]
            else:
                return None
        else:
            return None
        #
        # return (
        #     (await cls.insert_many(values_list=[value], with_return=with_return, on_conflict_do=on_conflict_do))[0]
        #     if with_return is True
        #     else await cls.insert_many(values_list=[value], with_return=with_return, on_conflict_do=on_conflict_do)
        # )

    @classmethod
    async def get(cls, *, primary_key: str, raise_on_exc: bool = False) -> PydanticGINOMixin:
        """
        Retrieve row by it's primary key

        :param primary_key: primary key value
        :param raise_on_exc: if "True", raises exceptions in case of errors. Otherwise, ignoring them.
        :return: serialized model
        """

        return await cls._get(primary_key=primary_key, raise_on_exc=raise_on_exc)

    @classmethod
    @_serialize(clsmethod=True)
    async def _get(cls, primary_key: str, raise_on_exc: bool = False) -> PydanticGINOMixin:

        return await cls._retrieve_one(**{cls._default_primary_key_attr: primary_key})

    @classmethod
    async def retrieve_one(cls, **where) -> Optional[PydanticGINOMixin]:
        """
        Retrieve one row thar corresponding to given params

        :param where: values from which the columns to be affected by the operation are selected
        :return: serialized model
        """

        return await cls._retrieve_one(**where)

    @classmethod
    @_serialize(clsmethod=True)
    async def _retrieve_one(cls, **where) -> Optional[PydanticGINOMixin]:

        if where == {} or where is None:
            raise ValueError('Unable to update row without specifying conditions: "where" is empty variable.')

        query = select(cls._bind)
        query = cls._add_where(query, where=where)

        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                result = (await conn.execute(query)).first()
        await conn.close()
        return result

    @classmethod
    async def retrieve_all(cls, **where) -> List[Any]:  # PydanticGINOMixin
        """
        Retrieve all rows thar corresponding to given params

        :param where: values from which the columns to be affected by the operation are selected
        :return: list of serialized models
        """

        return await cls._retrieve_all(**where)

    @classmethod
    @_serialize(clsmethod=True)
    async def _retrieve_all(cls, **where) -> List[Any]:  # PydanticGINOMixin

        query = select(cls._bind)
        query = cls._add_where(query, where=where)
        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                result = (await conn.execute(query)).all()
        await conn.close()
        return result

    @classmethod
    async def update_one(
        cls, *, values: Dict[str, Any], where: Dict[str, Any], with_return: bool = False, raise_on_exc: bool = False
    ) -> Optional[PydanticGINOMixin]:
        """
        Update one row. You can easily insert values with dict(column=value) syntax

        Example:
        Model.update_one(
            values=Model.dict(word='fancy new value', number=1),
            where=Model.dict(id='1-234-56789-0')
        )

        :param values: names of attributes from "self" which values should be used to update row
        :param where: values from which the columns to be affected by the operation are selected
        :param with_return: if "True", returns updated serialized model
        :param raise_on_exc: if "True", raises exceptions in case of errors. Otherwise, ignoring them.
        :return: updated serialized model or None
        """

        return await cls._update_one(values=values, where=where, with_return=with_return, raise_on_exc=raise_on_exc)

    @classmethod
    @_serialize(clsmethod=True)
    async def _update_one(
        cls, values: Dict[str, Any], where: Dict[str, Any], with_return: bool = False, raise_on_exc: bool = False
    ) -> Optional[PydanticGINOMixin]:

        if values == {} or values is None:
            raise ValueError('Unable to update row: nothing to update, "values" is empty variable.')

        if where == {} or where is None:
            raise ValueError('Unable to update row without specifying conditions: "where" is empty variable.')

        # setting up start of the query
        query = update(cls._bind).values(**values)
        query = cls._add_where(query, where)

        # adding returning() clause
        if with_return is True:
            query = query.returning(cls._bind.__table__)

        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                if with_return is True:
                    models = (await conn.execute(query)).all()
                else:
                    models = None
                    await conn.execute(query)
        await conn.close()

        if models is None:
            return None
        elif isinstance(models, list) and len(models) == 1:
            return models[0]
        elif isinstance(models, list) and len(models) == 0:
            return None
        else:
            return models

    async def update_one_self(
        self,
        *,
        values: Optional[Union[Tuple[str, ...], List[str], Set[str]]] = None,
        where: Dict[str, Any] = None,
        with_primary_key: bool = True,
        with_return: bool = False,
    ) -> Optional[PydanticGINOMixin]:
        """
        Update a database record using the values contained in the class instance

        :param values: names of attributes from "self" which values should be used to update row
        :param where: values from which the columns to be affected by the operation are selected
        :param with_primary_key: if True, uses "_default_primary_key_attr" arg in "where"
        :param with_return: if "True", returns updated serialized model
        :return: updated serialized model or None
        """

        if with_primary_key is True:
            if where is None:
                where = {}
            try:
                where[self._default_primary_key_attr] = self.__getattribute__(self._default_primary_key_attr)
            except AttributeError as ae:
                raise AttributeError(
                    f'Primary key based on field "{self._default_primary_key_attr}" must exist'
                    f'when "with_primary_key" is True and "where" is None, but it is was not presented.'
                ) from ae
        else:
            if where == {} or where is None:
                raise ValueError('Unable to update row without specifying conditions: "where" is empty variable.')

        if values is None:  # performing an update based on all available information
            values = self._data_source.keys()

        # redirect update inside of already existing method
        return await self.update_one(
            values={attr: self.__getattribute__(attr) for attr in values}, where=where, with_return=with_return
        )

    @classmethod
    async def delete(cls, *, where: Dict[str, Any] = None, with_return: bool = False, raise_on_exc: bool = False):
        """
        Deletes one or many rows by given params

        :param where: values from which the columns to be affected by the operation are selected
        :param with_return: if "True", returns updated serialized model
        :param raise_on_exc: if "True", raises exceptions in case of errors. Otherwise, ignoring them.
        :return: None
        """

        return await cls._delete(where=where, with_return=with_return, raise_on_exc=raise_on_exc)

    @classmethod
    @_serialize(clsmethod=True)
    async def _delete(cls, where: Dict[str, Any] = None, with_return: bool = False, raise_on_exc: bool = False):

        if not where:  # where == {}
            raise ValueError('Unable to delete row without specified "where" param)')

        query = delete(cls._bind)
        query = cls._add_where(query, where=where)

        # adding returning() clause
        if with_return is True:
            query = query.returning(cls._bind.__table__)

        # performing delete
        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                if with_return is True:
                    models = (await conn.execute(query)).all()
                else:
                    await conn.execute(query)
                    models = None
        await conn.close()

        return models if models else None

    @classmethod
    async def select_one(cls, *columns_to_select, **where) -> RowProxy:
        """
        Select some specific values from table

        :param columns_to_select: columns to get, i.e. Model.select_one("email", id=1)
        :return: list of RowProxy with needed values
        """

        return await cls._select_one(*columns_to_select, **where)

    @classmethod
    async def _select_one(cls, *columns_to_select, **where) -> RowProxy:
        query = select(cls._bind).with_only_columns([getattr(cls._bind, c) for c in columns_to_select])
        query = cls._add_where(query, where=where)
        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                result = (await conn.execute(query)).first()
        await conn.close()
        return result

    @classmethod
    async def select_many(cls, *columns_to_select, **where) -> List[RowProxy]:
        """
        Select list of some specific values from table

        :param columns_to_select: columns to get, i.e. Model.select_many("email", status="active)
        :return: single RowProxy with needed values
        """

        return await cls._select_many(*columns_to_select, **where)

    @classmethod
    async def _select_many(cls, *columns_to_select, **where) -> List[RowProxy]:
        query = select(cls._bind).with_only_columns([getattr(cls._bind, c) for c in columns_to_select])
        query = cls._add_where(query, where=where)
        async with cls.get_engine().connect() as conn:
            async with conn.begin():
                result = (await conn.execute(query)).all()
        await conn.close()
        return result
