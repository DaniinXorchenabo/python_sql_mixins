from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.types import TypeDecorator

from .json_tools import orjson_dumps, orjson_loads

__all__ = ["ORJSONB"]


class ORJSONB(TypeDecorator):
    impl = JSONB

    def process_bind_param(self, value, dialect):
        if value is None:
            return None

        return orjson_dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return None

        return orjson_loads(value)
