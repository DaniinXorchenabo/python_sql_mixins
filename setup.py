from setuptools import setup, find_packages

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

with open("requirements.txt", "r") as requirements_file:
    requirements = [i.strip() for i in requirements_file.readlines() if bool(i)]

setup(
    name="python-sql-mixins",
    version="0.0.1",
    author="George Chamor, Dmitry Finogeev, Daniil Dyachkov",
    author_email="dmitry.finogeev@relsys.eu",
    description="A mixins",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.newwheel.org/sbryakin/python-sql-mixins",
    packages=find_packages(),
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
